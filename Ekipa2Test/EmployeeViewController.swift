//
//  ViewController.swift
//  Ekipa2Test
//
//  Created by Tomaz Golob on 29/03/2017.
//  Copyright © 2017 Tomaz Golob. All rights reserved.
//

import UIKit
import Foundation

class EmployeeTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var wageLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
}

class EmployeeTableViewController: UITableViewController {
    
    // starting employees
    var employeesName: [String] = ["John", "Jack", "Alice", "Dell", "Darcy", "Ashton", "Cortney", "Ariel", "Logan"]
    var employeesSurname: [String] = ["Sappington", "Long", "Boatwright", "Ellison", "Bartham", "Rodgers", "Aaron", "Tyson", "Jansen"]
    var employeesID: [Int] = [10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009]
    var employeesBirthday: [String] = ["13/10/1941", "21/05/1947", "03/04/1956", "17/01/1965", "25/07/1974", "15/01/1987", "02/08/1990", "08/07/1991", "09/06/1996"]
    var employeesSalary: [Double] = [2441.0, 1968.0, 2106.0, 1277.0, 1331.0, 2460.0, 2295.0, 2485.0, 2933.0]
    var employeesRatings: [Double] = [58, 86, 84, 59, 91, 98, 78, 95, 67]
    //var employeesEvaluations: [Int] = [12, 16, 4, 13, 7, 2, 21, 6, 9]
    
    var employees: [Employee] = []
    var idNum: Int = 10010
    var editingEmployeeNum: Int = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildCompany()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }


    func buildCompany() {
        for i in 0...8 {
            let set = Employee(id: employeesID[i], name: employeesName[i], surname: employeesSurname[i], birthDay: employeesBirthday[i], salary: employeesSalary[i], rating: employeesRatings[i])
            self.employees.append(set)
        }
    }
    
    //MARK: - TableView Data Sources
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeCell", for: indexPath) as! EmployeeTableViewCell
        let emp = employees[indexPath.row]
        let employeeName = String(format: "%@ %@", emp.name, emp.surname)
        cell.nameLabel?.text = employeeName
        cell.idLabel.text = String(format: "id %5.d", emp.id)
        cell.dateLabel.text = emp.birthDay
        cell.wageLabel.text = String(format: "%.0f€/mo", emp.salary)
        cell.ratingLabel.text = String(format: "%2.1f", emp.rating/10)
        
        return cell
    }
    
    //add edit and delete swipe options for employees
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.employees.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.editingEmployeeNum = -1
        }
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            //TODO support editing
            self.editingEmployeeNum = indexPath.row
            self.performSegue(withIdentifier: "editEmployee", sender: nil)
        }
        
        edit.backgroundColor = UIColor(red: 21/255, green: 105/255, blue: 199/255, alpha: 1)
        return [delete, edit]
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "addEmployee":
            
            guard let employeeDetailViewController = segue.destination as? AddViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            employeeDetailViewController.nextID = self.idNum
            break
        case "editEmployee":
            
            guard let employeeDetailViewController = segue.destination as? AddViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            employeeDetailViewController.employee = employees[editingEmployeeNum]
            
            
        case "company":
            
            guard let companyDetailViewController = segue.destination as? ViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            var maxRating = 0.0
            var wages = 0.0
            var age = 0.0
            for emp in employees {
                wages = wages + emp.salary
                age = age + emp.age
                if maxRating < emp.rating {
                    maxRating = emp.rating
                }
            }
            companyDetailViewController.aW = wages/Double(employees.count)
            companyDetailViewController.aA = age/Double(employees.count)
            companyDetailViewController.mR = maxRating
            
            break
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }
    
    //MARK: Actions
    
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? AddViewController, let employee = sourceViewController.employee {
            
            if editingEmployeeNum != -1 {
                // Update an existing meal.
                employees[editingEmployeeNum] = employee
                editingEmployeeNum = -1
                tableView.reloadData()
            }
            else {
                // Add a new employee
                let newIndexPath = IndexPath(row: employees.count, section: 0)
                
                employees.append(employee)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
                if idNum == employee.id {
                    idNum = idNum + 1
                }
            }
        }
    }
    
    
    

    
    
}
