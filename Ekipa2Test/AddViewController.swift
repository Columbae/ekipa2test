//
//  AddViewController.swift
//  Ekipa2Test
//
//  Created by Tomaz Golob on 30/03/2017.
//  Copyright © 2017 Tomaz Golob. All rights reserved.
//

import UIKit
import os.log

class AddViewController: UIViewController, UITextFieldDelegate,  UINavigationControllerDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    
    @IBOutlet weak var idTextField: UITextField!
    
    @IBOutlet weak var salaryTextField: UITextField!
    
    @IBOutlet weak var ratingTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var employee: Employee?
    var nextID: Int = 0
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
        idTextField.text = String(describing: nextID)
        
        // Set up views if editing an existing Meal.
        if let employee = employee {
            navigationItem.title = "Edit employee"
            nameTextField.text = employee.name
            surnameTextField.text = employee.surname
            birthdayTextField.text = employee.birthDay
            idTextField.text = String(employee.id)
            salaryTextField.text = String(employee.salary)
            ratingTextField.text = String(employee.rating)
        }
        
        nameTextField.delegate = self
        nameTextField.tag = 0
        surnameTextField.delegate = self
        surnameTextField.tag = 1
        birthdayTextField.delegate = self
        birthdayTextField.tag = 2
        idTextField.delegate = self
        idTextField.tag = 3
        salaryTextField.delegate = self
        salaryTextField.tag = 4
        ratingTextField.delegate = self
        ratingTextField.tag = 5
    }
    
    //keyboard
    //hide
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //next field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        let name = nameTextField.text ?? ""
        let surname = surnameTextField.text ?? ""
        let birthday = birthdayTextField.text ?? ""
        let id = Int(idTextField.text!) ?? 0
        let salary = Double(salaryTextField.text!) ?? 0.0
        let rating = Double(ratingTextField.text!) ?? 0.0
        employee = Employee(id: id, name: name, surname: surname, birthDay: birthday, salary: salary, rating: rating)
    }
}
