//
//  Employee.swift
//  Ekipa2Test
//
//  Created by Tomaz Golob on 30/03/2017.
//  Copyright © 2017 Tomaz Golob. All rights reserved.
//

import Foundation

extension Date {
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}

class Employee: Equatable, Hashable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    static func ==(lhs: Employee, rhs: Employee) -> Bool {
        return lhs.id == rhs.id
    }
    
    var name: String
    var surname: String
    var id: Int
    var birthDay: String
    var salary: Double
    var rating: Double
    
    var age: Double
    //var evaluations: Int
    
    init(id: Int, name: String, surname: String?, birthDay: String, salary: Double, rating: Double?) {
        self.id = id
        self.name = name
        self.surname = surname ?? ""
        self.birthDay = birthDay
        self.salary = salary
        self.rating = rating ?? 0
        
        let birthdayArray = birthDay.components(separatedBy: "/")
        let day = Int(birthdayArray[0]) ?? 0
        let month = Int(birthdayArray[1]) ?? 0
        let year = Int(birthdayArray[2]) ?? 0
        let dob = Calendar.current.date(from: DateComponents(year: year, month: month, day: day))!
        
        age = Double(dob.age)
        //self.evaluations = evaluations ?? 0
    }
    
    var hashValue: Int {
        get {
            return id.hashValue
        }
    }
}
