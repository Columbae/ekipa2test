//
//  ViewController.swift
//  Ekipa2Test
//
//  Created by Tomaz Golob on 30/03/2017.
//  Copyright © 2017 Tomaz Golob. All rights reserved.
//

//import Foundation
import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var avgWage: UILabel!
    @IBOutlet weak var avgAge: UILabel!
    @IBOutlet weak var maxRating: UILabel!
    
    var aW: Double = 0.0
    var aA: Double = 0.0
    var mR: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avgWage.text = String(format: "%.0f€/mo", aW)
        avgAge.text = String(format: "%.1f", aA)
        maxRating.text = String(format: "%2.1f", mR/10)
    }
}
